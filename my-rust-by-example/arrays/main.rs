use std::mem;

fn analyze_slice(slice: &[i32]) {
  println!("first element of the slice: {}", slice[0]);
  println!("the slice has {} elements", slice.len());
}

fn main() {
  let xs: [i32; 5] = [1, 2, 3, 4, 5];

  let ys: [i32; 5] = [1, 2, 3, 4, 5];

  println!("\nAll elements: {:?}", xs);
  println!("\nfirst element: {}", xs[0]);
  println!("\nsecond element: {}", xs[1]);

  println!("\narray size: {}", xs.len());

  println!("\nStack allocation: {}", mem::size_of_val(&xs));

  println!("\nBorrow the whole thing as a slice");
  analyze_slice(&xs);

  println!("\nBorrow a slice");
  analyze_slice(&xs[1..4]);
  analyze_slice(&ys[1..4]);
}