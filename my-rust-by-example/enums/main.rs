// An attribute to hide warnings for unused code.
#![allow(dead_code)]

// Create an `enum` to classify a web event. Note how both
// names and type information together specify the variant:
// `PageLoad != PageUnload` and `KeyPress(char) != Paste(String)`.
// Each is different and independent.
enum WebEvent {
    // An `enum` may either be `unit-like`,
    PageLoad,
    PageUnload,
    // like tuple structs,
    KeyPress(char),
    Paste(String),
    // or like structures.
    Click { x: i64, y: i64 },
}

enum Status {
  Rich,
  Poor
}

enum Work {
  Employed,
  Unemployed
}

enum Number {
  Zero,
  One, 
  Two
}

enum Color {
  Red = 0xff0000,
  Green = 0x00ff00,
  Blue = 0x0000ff,
}

fn inspect(event: WebEvent) {
  match event {
    WebEvent::PageLoad      => println!("Page Loaded"),
    WebEvent::PageUnload    => println!("Page Unloaded"),
    WebEvent::KeyPress(c)   => println!("Pressed: '{}'.", c),
    WebEvent::Paste(s)      => println!("Pasted \"{}\".", s),
    WebEvent::Click { x, y} => {
      println!("Clicked at x={}, y={}.", x, y);
    },
  }
}

fn main() {
  let pressed = WebEvent::KeyPress('x');
  let pasted = WebEvent::Paste("words".to_owned());
  let clicked = WebEvent::Click{ x: 4, y: 20 };
  let loaded = WebEvent::PageLoad;
  let unloaded = WebEvent::PageUnload;

  inspect(pressed);
  inspect(pasted);
  inspect(clicked);
  inspect(loaded);
  inspect(unloaded);

  println!("\nScoping enums with `use`");
  use crate::Status::{Rich, Poor};
  use crate::Work::*;

  let status = Poor;
  let work = Unemployed;

  match status {
    Rich => println!("Gots monies"),
    Poor => println!("Gots no monies"),
  }

  match work {
    Employed => println!("Hi Ho, Hi Ho"),
    Unemployed => println!("Will program for food"),
  }

  println!("\nC-like enums");
  //casting enums to numeric
  println!("zero is {}", Number::Zero as i32);
  println!("one is {}", Number::One as i8);

  println!("Roses are #{:06x}", Color::Red as u32);
  println!("Violets are #{:06x}", Color::Blue as i64);
}