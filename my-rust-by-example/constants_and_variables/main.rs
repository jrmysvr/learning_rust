// Globals are declared outside all other scopes
static LANGUAGE: &str = "Rust";
// Consts cannot be modified
const THRESHOLD: i32 = 10;

fn is_big(n: i32) -> bool {
    // Global scope variables are accessible 
    n > THRESHOLD
}

fn main() {
    println!("Constants");
    let n: i32 = 16;
    println!("This is {}", LANGUAGE);
    println!("The threshold is {}", THRESHOLD);
    println!("{} is {}", n, if is_big(n) {"Big"} else {"Not Big"});

    // type annotations aren't always necessary - the compiler can infer
    let an_integer = 1u32;
    let a_boolean = true;
    let unit = ();
    
    println!("\nVariables");
    println!("An integer: {:?}", an_integer);
    println!("A boolean: {:?}", a_boolean);
    println!("A unit value: {:?}", unit);

    let copied_integer = an_integer;
    println!("\nIt's possible to copy variables: {} copied to {}", an_integer, copied_integer);

    //Avoid warnings about unused variables with an underscore prefix
    let _unused_variable = 3u32;
    let _some_other_variable = "words";

    println!("\nMutability");
    let mut mutable_binding = 1;
    println!("This variable is mutable: {}", mutable_binding);

    mutable_binding += 1;
    println!("New Value: {}", mutable_binding);
}