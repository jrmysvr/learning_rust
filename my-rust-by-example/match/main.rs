
fn match_number(number: u32) {

    let msg = format!("Tell me about {}:", number);
    print!("\t{:<20}", msg);
    match number {
        1 => println!("One!"),
        2 | 3 | 5 | 7 | 11 => println!("This is a prime via guards"),
        13...19 => println!("Teen (inclusive range)"),
        _ => println!("Default")
    }
}

fn binary_from_boolean(boolean: bool) -> u8 {
   let output = match boolean {
       false => 0,
       true => 1
   }; 

   output
}

fn match_pair(pair: (i32, i32)) {
    println!("Working with {:?}", pair); 

    let format = match pair {
        (0, _) => format!("The first value was matched"),
        (x, 0) => format!("The first value is {}, and the second value was matched", x),
        _ => format!("Default - no match")
    };

    println!("\t{}",format);
}

enum Color {
    RED,
    GREEN,
    BLUE,
    RGB(u32, u32, u32),
}

fn match_color(color: Color) {
    println!("What color is it?");
    match color {
        Color::RED  => println!("Angry Red"),
        Color::GREEN => println!("Envious Green"),
        Color::BLUE => println!("Depressive Blue"),
        Color::RGB(r,g,b) => println!("Destructured RGB: ({},{},{})", r, g, b),
    }
}

fn main() {
    for num in 1..20 {
        match_number(num);
    }

    let bools = vec![true, false, false, true];
    let mut bins = vec![];
    println!("\nVector of boolean values: {:?}", bools);
    for b in bools.into_iter() {
        bins.push(binary_from_boolean(b));
    }

    println!("\nBooleans converted to binary digits: {:?}", bins);

    println!("\nDestructuring with match");
    let pairs = vec![(0, -2), (1, 0), (-1, -1)];
    for p in pairs.into_iter() {
        match_pair(p);
    }

    println!("\nDestructuring an enum");
    let colors = vec![Color::RGB(255,50,20), Color::BLUE, Color::GREEN, Color::RED];
    for c in colors.into_iter() {
        match_color(c);
    }
}
