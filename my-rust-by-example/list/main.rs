
use std::fmt;


struct List(Vec<i32>);

impl fmt::Display for List {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    let vec = &self.0;

    // Start open bracket
    write!(f, "[")?;

    for (count, v) in vec.iter().enumerate() {
      write!(f, "{}: {}", count, v)?;
      if count != vec.len()-1 {write!(f, ", ")?;}

      // if count != 0 {write!(f, ", ")?;}
      // write!(f, "{}", v)?;
      // try!(write!(f, "{}", v));
    }

    // Return Result
    write!(f, "]")
  }
}

fn main() {
  let v = List(vec![1, 2, 3]);
  println!("{}", v);
}