use std::fmt::{self, Formatter, Display};

fn reverse(pair: (i32, (i32, bool))) -> ((bool, i32), i32) {
  // destructuring
  let (first, (second, third)) = pair;
  ((third, second), first)
}

fn transpose(matrix: Matrix) -> Matrix {

  Matrix(matrix.0, matrix.2,
         matrix.1, matrix.3)
  // let ((a, b), 
  //      (c, d)) = ((matrix.0, matrix.1), 
  //                 (matrix.2, matrix.3));

  // Matrix(a, c, b, d)
  
}

#[derive(Debug)]
struct Matrix(f32, f32, f32, f32);

impl Display for Matrix {
  fn fmt(&self, f: &mut Formatter) -> fmt::Result {
    writeln!(f,"
    ( {:.2} {:.2} ) 
    ( {:.2} {:.2} )", 
    self.0, self.1, 
    self.2, self.3)
  }
}

fn main() {
  let long_tuple = (1u8, 2u16, 3u32, 4u64, 
                    -1i8, -2i16, -3i32, -4i64,
                    0.1f32, 0.2f64,
                    'a', true);

  println!("long tuple first value: {}", long_tuple.0); // tuple access `.`
  println!("long tuple second value: {}", long_tuple.1);

  let tuple_of_tuples = ((1u8, 2u16, 2u32), long_tuple);
  println!("tuple of tuples: {:?}", tuple_of_tuples);

  println!("Long tuples are problematic"); 
  // println!("{:?}", (1,2,3,4,5,6,7,8,9,10,11,12,13)); //breaks
  println!("{:?}", (1,2,3,4,5,6,7,8,9,10,11,12)); //works

  let pair = (1, (2, false));
  println!("Reverse tuple: {:?} => {:?}", pair, reverse(pair));

  println!("Tuple: {:?}, Not Tuple: {}", ("HI", ), ("HI"));

  let matrix = Matrix(1.1, 1.2, 2.1, 2.2);
  println!("{:?}", matrix);
  println!("Matrix: {}", matrix);
  println!("Transpose: {}", transpose(matrix));
}