use std::fmt;

// Using a type alias
// type Centimeters = u32;

// Using a struct
struct Centimeters(u32);

impl fmt::Display for Centimeters {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} cm", self.0)
    }
}

struct Circle {
    radius: Centimeters
}

impl fmt::Display for Circle {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Circle with radius: {}", self.radius)
    }
}
fn main() {
    println!("To String");
    // Centimeters alias
    // let circle = Circle { radius: 1_000 };
    // Centimeters struct
    let circle = Circle {radius: Centimeters(1000)};
    println!("{}", circle.to_string());

    println!("\nParsing A String");
    let parsed: i32 = "5".parse().unwrap();
    let turbo_parsed = "10".parse::<i32>().unwrap();

    let sum = parsed + turbo_parsed;
    println!("Sum of parsed values: {}", sum);
}