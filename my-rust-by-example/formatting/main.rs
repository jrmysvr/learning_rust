use std::fmt::{self, Formatter, Display};

#[derive(Debug)]
struct City {
  name: &'static str,
  lat: f32, 
  lng: f32,
}

impl Display for City {
  // `f` is a buffer, this method writes the formatted string to it 
  fn fmt(&self, f: &mut Formatter) -> fmt::Result {
    let lat_c = if self.lat >= 0.0 {'N'} else {'S'};
    let lng_c = if self.lng >= 0.0 {'E'} else {'W'};

    write!(f, "{:>10}: {:>7.3}deg {} {:>7.3}deg {}",
           self.name, 
           self.lat.abs(), lat_c,
           self.lng.abs(), lng_c)
  }
}

#[derive(Debug)]
struct Color {
  red: u8,
  green: u8,
  blue: u8
}

impl fmt::Display for Color {
  fn fmt(&self, f: &mut Formatter) -> fmt::Result {
    write!(f, "RGB ({0}, {1}, {2}) 0x{0:02X}{1:02X}{2:02X}",
              self.red,
              self.green,
              self.blue)
  }
}


fn main() {
  for city in [
    City { name: "Dublin", lat: 53.347778, lng: -6.259722 },
    City { name: "Oslo", lat: 59.95, lng: 10.75 },
    City { name: "Vancouver", lat: 49.25, lng: -123.1 },
  ].iter() {
    println!("{}", *city);
  }

  println!("#######################################");
  for color in [
    Color { red: 128, green: 255, blue: 90 },
    Color { red: 0, green: 3, blue: 254 },
    Color { red: 0, green: 0, blue: 0 },
  ].iter() {
    println!("{}", *color)
  }
}
