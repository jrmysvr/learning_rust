#[derive(Debug)]
struct Person<'a> {
  name: &'a str,
  age: u8,
}

struct Nil;

struct Pair(i32, f32);

#[derive(Debug)]
struct Point {
  x: f32,
  y: f32,
}

//Fields can also be structs
#[allow(dead_code)]
#[derive(Debug)]
struct Rectangle {
  p1: Point,
  p2: Point,
}

fn rect_area(rect: &Rectangle) -> f32 {
  let Point{ x: x1, y: y1} = rect.p1;
  let Point{ x: x2, y: y2} = rect.p2;

  (x2 - x1) * (y2 - y1)
}

fn square(point: Point, side: f32) -> Rectangle {
  Rectangle {
    p1: Point { ..point },
    p2: Point { x: point.x + side, y: point.y + side }
  }
}

fn main() {
  let name = "Jeremy";
  let age = 29;
  let jeremy = Person { name, age };

  println!("{:#?}", jeremy);

  let point: Point = Point { x: 0.3, y: 0.4 };
  println!("\nPoint coordinates: ({}, {})", point.x, point.y);

  let new_point = Point { x: 0.1, ..point };
  println!("\nDestructured update struct: ({}, {})", new_point.x, new_point.y);

  let Point { x: my_x, y: my_y } = point;
  println!("\nDestructured fields: my_x={}, my_y={}", my_x, my_y);

  let _nil = Nil;

  let pair = Pair(1, 0.1);

  println!("\nPair contains {:?} and {:?}", pair.0, pair.1);

  let Pair(integer, decimal) = pair;

  println!("Destructured tuple struct: {:?} and {:?}", integer, decimal);

  let _rectangle = Rectangle {
    p1: Point { x: my_x, y: my_y },
    p2: new_point,
  };

  println!("\nRectangle: {:#?}", _rectangle);
  println!("\nRectangle area: {}", rect_area(&_rectangle));

  let _other = Rectangle {
    p1: Point { x: 1., y: 2. },
    p2: Point { x: 2., y: 3. }
  };

  println!("\nAnother Rectangle {:#?}", _other);
  println!("\nRectangle area: {}", rect_area(&_other));

  let _square = square(point, 5.);
  println!("\nA Square: {:#?}", _square);
}