
use std::convert::From;

#[derive(Debug)]
struct Number {
    value: i32,
}

impl From<i32> for Number {
                    // not `self`
    fn from(item: i32) -> Self {
        Number { value: item }
    }
}

fn main() {
    println!("From");
    let num = Number::from(30);
    println!("My number is {:?}", num);

    let string = "sup";
    let new_string = String::from(string);

    println!("First string: {}",string);
    println!("New String: {:?}", new_string);

    println!("\nInto");
    // let int = 5;
    // let num: Number = int.into();
    let num: Number = 5.into();
    println!("My number is {:?}", num);
}