
fn main() {
    
    println!("{} stringified", 42);

    println!("{0}, {2}, {1}", "Ordering", "Numbers", "Using");
    
    println!("{named} {arguments} {help}", named="Ordering",
                                           help="named arguments",
                                           arguments="using");

    println!("Special formatting: {} of {:b} people know binary, the other half doesn't", 1, 2);


    println!("Right alignment: {text:>width$}", text="words", width=10);

    println!("Padding: {number:>0width$}", number=1, width=10);

    println!("Decimal Formatting: {pi:0.3}", pi=3.141592);

    #[allow(dead_code)]
    #[derive(Debug)]
    struct Structure(i32);

    println!("This struct `{:?}` won't print... unless you derive", Structure(3));
}
