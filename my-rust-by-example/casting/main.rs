// Suppress all warnings from casts which overflow.
#![allow(overflowing_literals)]

fn main() {
    let decimal = 65.4321_f32;
    // Nope
    // let integer: u8 = decimal;    

    // Yep
    let integer = decimal as u8;
    let character = integer as char;

    println!("Casting");
    println!("We were casted: {} -> {} -> {}", decimal, integer, character);

    let value = 1000;
    println!("\nCasting bigger values");
    println!("{} as a u16 is :{}", value, value as u16);    
    println!("{} as a u8 is {}", value, value as u8);
    println!("{} mod 256 is {}", value, value % 256);
    println!("\nCasting negative values");
    println!("{} as a i8 is {}", -1, -1 as i8);
    println!("{} as a u8 is {}", -1, (-1i8) as u8);

    let value = 128;
    // When casting to a signed type, the (bitwise) result is the same as
    // first casting to the corresponding unsigned type. If the most significant
    // bit of that value is 1, then the value is negative.
    println!("\nCasting between signed and unsigned types");
    println!("{} as i16 is {}", value, value as i16);
    println!("{} as i8 is {}", value, value as i8);
    // two's complement
    println!("{} as a i8 is {}", 232, 232 as i8);
}