
#[derive(Debug)]
struct Structure(i32);

#[derive(Debug)]
struct Another(Structure);

#[derive(Debug)]
struct PrettyGirl<'a> {
    name: &'a str,
    age: u8,
}

fn main() {

    let name = "Stephanie";
    let age = 26;
    let schtephli = PrettyGirl {name, age};

    println!("{:?} is a number", -1);
    println!("{:?} is a string", "words");
    println!("{:#?} is a string, pretty printed", "words");

    println!("{:?} is a custom structure", Structure(-1));
    println!("{:?} is another custom structure", Another(Structure(-1)));
    println!("{:#?} is another custom structure, pretty printed", Another(Structure(-1)));

    println!("{:#?}", schtephli);
}