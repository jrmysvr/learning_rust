type Nanosecond = u64;
type Inch = u64;

#[allow(non_camel_case_types)]
type u64_t = u64;

fn main() {
    let nanoseconds: Nanosecond = 5;
    let inches: Inch = 2;

    println!("New Types");
    println!("{} nanoseconds + {} inches = {}?",
             nanoseconds,
             inches,  
             nanoseconds + inches);

    let nanoseconds: Nanosecond = 5 as u64_t;
    let inches: Inch = 2 as u64_t;

    println!("Aliased new types");
    println!("{} nanoseconds + {} inches = {}?",
             nanoseconds,
             inches,  
             nanoseconds + inches);
}