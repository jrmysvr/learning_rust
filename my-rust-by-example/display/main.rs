
use std::fmt;

#[derive(Debug)]
struct PrettyGirl<'a> {
    name: &'a str,
    age: u8
}

impl<'a> fmt::Display for PrettyGirl<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Pretty Girl whose name is {}", self.name)
    }
}

#[derive(Debug)]
struct MinMax(i64, i64);  

impl fmt::Display for MinMax {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({}, {})", self.0, self.1)
    }
}

impl fmt::Binary for MinMax {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({:b}, {:b})", self.0, self.1)
    }
}

#[derive(Debug)]
struct Complex {
    real: f64,
    imag: f64
}

impl fmt::Display for Complex {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} + {}i", self.real, self.imag)
    }
}

fn main() {

    let name = "Stephanie";
    let age = 26;
    let schtephli = PrettyGirl { name, age };

    println!("{:#?}", schtephli);
    println!("{}", schtephli);

    let big_range = MinMax(-300, 300);
    let small_range = MinMax(-3, 3);

    println!("Big Range: {big}, \tSmall Range: {small}",
             small=small_range,
             big=big_range);
    println!("Binary Range: {small:b}", small=small_range);

    let complex = Complex{real:3.3, imag:7.2};
    println!("Display: {}", complex);
    println!("Debug: {:?}", complex);
    println!("Debug (pretty): {:#?}", complex);


}