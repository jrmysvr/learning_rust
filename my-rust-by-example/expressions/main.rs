
#[allow(dead_code)]
fn fizz_buzz_or_num(n: u32) -> String{
    let mut output: &str = &format!("{}", n);
    if n % 15 == 0 {
       output = "fizzbuzz"; 
    } else if n % 3 == 0 {
        output = "fizz";
    } else if n % 5 ==0 {
        output = "buzz";
    }

    output.to_string()
}

#[allow(unreachable_code)]
#[allow(dead_code)]
fn loop_with_break() {
    let mut count = 0u32;
    let max = 10_000u32;
    println!("Loop with break");
    loop {
        count += 1;

        if count == 10 {
            println!("Count is {}", count);
            continue;
        }

        if count % 501 == 0 {
            println!("Count is {}", count);
        } else if count == max {
            println!("Count is maxed out at {}", count);
            break;
        }
    }
}

#[allow(unreachable_code)]
#[allow(dead_code)]
fn loop_with_nested_labels() {
    let mut count = 0u32;
    println!("\nLoop with nested labels");
    'outer: loop {
        println!("Outer loop");
        'inner: loop {
            println!("Inner loop");
            if count == 2 {
                break 'outer;
            }
            println!("Bottom of Inner loop");
            count += 1;
        }
        println!("Bottom of Outer loop");
    }
}

#[allow(unreachable_code)]
#[allow(dead_code)]
fn return_from_break() {
    let mut count = 0u32;
    let max = 10_000u32;
    println!("\nReturn from break");
    let result = loop {
        count += 1;
        if count == max {
            break count * 2;
        }
    };
    println!("Return value from loop: {}", result);
    assert_eq!(result, 2*max);
}

#[allow(dead_code)]
fn while_loop() {
    let mut n = 1u32;
    println!("\nWhile loop");
    while n < 21 {
        println!("{}", fizz_buzz_or_num(n));
        n += 1;
    }
}

#[allow(dead_code)]
fn for_loops() {
    println!("\nFor FizzBuzz");
    // for n in 1..21 {
    for n in 1..=20 { // inclusive range
        println!("{}", fizz_buzz_or_num(n));
    }

    let names1 = vec!["Jeremy", "Stephanie", "Heinz", "Luzia", "Stefan"];
    let names2 = names1.clone();
    let mut names3 = names1.clone();

    println!("\nFor iters");
    println!("Borrowed iter");
    for name in names1.iter() {
        match name {
            &"Stephanie" => println!("Hoi, dear"),
            _ => println!("Yo, {}", name),
        }
    }

    println!("Length of names: {}", names1.len());

    println!("Consumed Iter");
    for name in names2.into_iter() {
        match name {
            "Stephanie" => println!("Hoi, dear"),
            _ => println!("Yo, {}", name),
        }
    }

    // Throws an error - use of moved value
    // println!("Length of names: {}", names2.len());

    println!("Mutable Iter");
    for name in names3.iter_mut() {
        *name = match name {
            &mut "Stephanie" => "Schtephli",
            _ => "Not Sctephli",
        }
    }
    println!("Mutated names: {:?}", names3);

}

fn main() {
    loop_with_break();
    loop_with_nested_labels();
    return_from_break();
    while_loop();
    for_loops();
}