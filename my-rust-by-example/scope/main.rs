fn main() {
    let long_lived = 1;
    {
        let short_lived = 2;
        println!("Inside a scope: {}", short_lived);

        let long_lived = 100;
        println!("Inside scope, long lived: {}", long_lived)
    }

    println!("Long lived: {}", long_lived);

    let long_lived = "shadowed";
    println!("Shadowed binding: {}", long_lived);
}